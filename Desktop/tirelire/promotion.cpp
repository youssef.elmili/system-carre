#include "promotion.h"
#include<QMessageBox>
#include<QPainter>
#include<QPdfWriter>
#include<QSystemTrayIcon>

Promotion::Promotion()
{

}
Promotion::Promotion (int ID , QString Date_Promo,QString promo )
{
    this->ID=ID;
    this->date_promo=Date_Promo ;
    this-> promo=promo ;
}


bool Promotion::ajouter()
{
    QSqlQuery query ;
    QString ress=QString::number(ID);
    query.prepare("insert into PROMOTION (ID_PROMOTION,DATE_POMO,PROMOTION) " "values(:id,:dp,:p)"); //commande

    query.bindValue(":id",ress);
    query.bindValue(":dp",date_promo);
    query.bindValue(":p",promo );

    return query.exec();
}



QSqlQueryModel *Promotion::afficher()
{
    QSqlQueryModel * model = new QSqlQueryModel ();


    model->setQuery("select *from PROMOTION"); //selection de la table à afficher
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID_PROMOTION"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("DATE_POMO"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("PROMOTION"));

    return model;


}


bool Promotion::supprimer(int id)
{  //supprimer suivant l'id
    QSqlQuery query ;
    QString ress=QString::number(id);
    query.prepare("delete from PROMOTION where ID_PROMOTION=:id");
    query.bindValue(":id",ress);
    return query.exec();

}

bool Promotion::modifier()
{
    QSqlQuery query ;
    QString ress=QString::number(ID);
    query.prepare("update PROMOTION set DATE_POMO=:dp,PROMOTION=:P where ID_PROMOTION=:id");

    query.bindValue(":id",ress);
    query.bindValue(":dp",date_promo);
    query.bindValue(":P",promo);


    return query.exec();

}




QSqlQueryModel *  Promotion::trie(const QString &critere, const QString &mode )
{
    QSqlQueryModel * model= new QSqlQueryModel();

model->setQuery("select * from PROMOTION order by "+critere+" "+mode+""); //taayet il tab il mawjoud f base donnée
model->setHeaderData(0, Qt::Horizontal, QObject::tr("ID_PROMOTION"));   //on est entrain de modifier par rapport au numéro de case
model->setHeaderData(1, Qt::Horizontal, QObject::tr("DATE_POMO "));
model->setHeaderData(2, Qt::Horizontal, QObject::tr("PROMOTION"));
return  model ;
}

QSqlQueryModel * Promotion::rechercher (const QString &aux)
{
QSqlQueryModel * model = new QSqlQueryModel();

  model->setQuery("select * from PROMOTION where ((ID_PROMOTION || DATE_POMO || PROMOTION ) LIKE '%"+aux+"%') ");
  model->setHeaderData(0, Qt::Horizontal, QObject::tr("ID_PROMOTION"));
  model->setHeaderData(1, Qt::Horizontal, QObject::tr("DATE_POMO"));
  model->setHeaderData(2, Qt::Horizontal, QObject::tr("PROMOTION "));

  return model;
}


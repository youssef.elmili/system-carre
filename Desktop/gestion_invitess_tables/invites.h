#ifndef INVITES_H
#define INVITES_H
#include <QString>
#include <QSqlQuery>
#include <QSqlQueryModel>

class invites
{ private :
    QString nom,prenom,email,appartenance;
    int id,membre ;

public:
    invites(){};
    invites(QString nom ,QString prenom,int id);
    invites(QString nom ,QString prenom,int id,int,QString,QString);

    void setnom(QString n) ;
    void setprenom(QString m) ;
    void setemail(QString m);
    void setappartenance(QString m);
    void setid(int c) ;

    QString getnom();
    QString getprenom();
    int getid();

    bool ajouter();
    QSqlQueryModel * afficher();
    QSqlQueryModel *afficher (QString checki,QString type);
    QSqlQueryModel *afficher_trier (QString type,QString facon);
    bool supprimer(int id) ;
    bool modifier();
    void imprimer_pdf_invitee_un_seul();

};

#endif // INVITES_H

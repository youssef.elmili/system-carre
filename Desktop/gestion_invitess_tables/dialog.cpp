#include "dialog.h"
#include "ui_dialog.h"
#include "connection.h"



Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    QPieSeries *series =new QPieSeries ;

    QList<qreal> list= stat_invitee();

        series->append("Ami de la femme",list[0]);
        series->append("Ami du mari",list[1]);
        series->append("Famille de la femme",list[2]);
        series->append("Famille du mari",list[3]);


        QChart *chart =new QChart;
        chart->addSeries(series);
        chart->setTitle("STATS des Invitées");
         QChartView *chartview= new QChartView(chart);
         chartview->setParent(ui->horizontalFrame);
}

Dialog::~Dialog()
{
    delete ui;
}

 QList<qreal> Dialog::stat_invitee()
{
    QList<qreal> list ;
    QSqlQuery query;
    query.prepare("select APARTENANCE, sum(MEMEBRE) as TotalSum from INVITEES group by APARTENANCE ORDER BY APARTENANCE ASC");
    query.exec();
    while(query.next())
    {list.append(query.value(1).toInt());}

  return list;

 }




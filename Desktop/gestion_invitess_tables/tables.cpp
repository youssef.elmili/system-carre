#include "tables.h"
#include <QMessageBox>
#include <QPdfWriter>
#include <QPainter>
#include <QSystemTrayIcon>
#include <QDateTime>




tables::tables()
{

}


tables::tables(int id , int numero ,int nombre_chaises,QString appartenance)
{

    this->ID_table=id;
    this->numero=numero;
    this->nombre_de_chaises=nombre_chaises;
    this->appartenance_table=appartenance;


}


bool tables::ajouter_table()
{
    QSqlQuery query;
    QString res = QString::number(ID_table);
    QString res2=QString::number(numero);
    QString res3 =QString::number(nombre_de_chaises);


    query.prepare("insert into TABLES (ID_TABLE ,NUMERO , NOMBRE_DE_CHAISES,APPARTENANCE_TABLE) " "values( :id, :numero, :nombre_chaise,:appartenance)");

    query.bindValue(":id",res);
    query.bindValue(":numero",res2);
    query.bindValue(":nombre_chaise",res3);
    query.bindValue(":appartenance",appartenance_table);


 return query.exec();
}






bool tables::modifier_table()
{

    QSqlQuery query;
    QString res = QString::number(ID_table);
    QString res2=QString::number(numero);
    QString res3 =QString::number(nombre_de_chaises);

    query.prepare("update TABLES set NUMERO=:num, NOMBRE_DE_CHAISES=:nombre,APPARTENANCE_TABLE=:appartenance where ID_TABLE= :id ");

    query.bindValue(":id",res);
    query.bindValue(":num",res2);
    query.bindValue(":nombre",res3);
    query.bindValue(":appartenance",appartenance_table);

 return query.exec();


}

void tables::imprimer_table_pdf_tables()
{
    QDateTime datecreation = QDateTime::currentDateTime();
    QString afficheDC = "Date de Creation PDF : " + datecreation.toString() ;
           QPdfWriter pdf("C:\\Users\\Youssef El Mili\\Desktop\\gestion_invitess_tables\\file_table_base.pdf");
           QPainter painter(&pdf);
          int i = 4000;
               painter.setPen(Qt::red);
               painter.setFont(QFont("ATLAS", 30));
               painter.drawText(4000,1500,"Liste Des Tables");
               painter.setPen(Qt::black);
               painter.setFont(QFont("Arial", 15));
               painter.drawText(100,12500,afficheDC);
               painter.drawRect(2600,70,7000,2600);
               QPixmap image(":/Images/images/logo.png");
               painter.drawPixmap(QRect(0,70,2600,2600),image);
               painter.drawRect(0,3000,9600,500);
               painter.setFont(QFont("Arial", 9));
               painter.drawText(200,3300,"ID_TABLE");
               painter.drawText(1300,3300,"NUMERO");
               painter.drawText(3000,3300,"NOMBRE_DE_CHAISES");
               painter.drawText(5000,3300,"APPARTENANCE_TABLE");




               QSqlQuery query;
               query.prepare("select * from TABLES");
               query.exec();
               while (query.next())
               {
                   painter.drawText(200,i,query.value(0).toString());
                   painter.drawText(1300,i,query.value(1).toString());
                   painter.drawText(3000,i,query.value(2).toString());
                   painter.drawText(5000,i,query.value(3).toString());


                  i = i + 450;
               }


int reponse = QMessageBox::Yes;
if (reponse == QMessageBox::Yes)
{
    QSystemTrayIcon *notifyIcon = new QSystemTrayIcon;
    notifyIcon->show();
    notifyIcon->setIcon(QIcon("icone.png"));

    notifyIcon->showMessage("Tables des Tables","La liste est prete",QSystemTrayIcon::Information,15000);

    painter.end();
}
if (reponse == QMessageBox::No)
{
     painter.end();
}
}


QSqlQueryModel * tables::afficher_table()
{
    QSqlQueryModel *model =new QSqlQueryModel();

    model->setQuery("select *from TABLES");
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID_TABLE"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("NUMERO"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("NOMBRE_DE_CHAISES"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("APPARTENANCE_TABLE"));


    return model ;
}

QSqlQueryModel *tables::afficher(QString checki, QString type)
{
    QSqlQueryModel *model =new QSqlQueryModel();
    QSqlQuery query ;

    if (type=="SANS CONDITION"){model->setQuery("Select * from TABLES where ID_TABLE like '%"+checki+"%' or NUMERO like '%"+checki+"%' or NOMBRE_DE_CHAISES like '%"+checki+"%' or APPARTENANCE_TABLE like '%"+checki+"%'");}

      else model->setQuery("Select * from TABLES where "+type+" like '%"+checki+"%' ");


    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID_TABLE"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("NUMERO"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("NOMBRE_DE_CHAISES"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("APPARTENANCE_TABLE"));
    return model ;
}




QSqlQueryModel *tables::afficher_trier(QString type, QString facon)
{
    QSqlQueryModel *model =new QSqlQueryModel();
         QSqlQuery query ;


          model->setQuery("SELECT * FROM TABLES ORDER BY "+type+" "+facon+"");
          model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID_TABLE"));
          model->setHeaderData(1,Qt::Horizontal,QObject::tr("NUMERO"));
          model->setHeaderData(2,Qt::Horizontal,QObject::tr("NOMBRE_DE_CHAISES"));
          model->setHeaderData(3,Qt::Horizontal,QObject::tr("APPARTENANCE_TABLE"));
           return model ;
}







bool tables::supprimer_table(int id)
{
    QSqlQuery query ;
    QString res=QString::number(id);

    query.prepare("Delete from TABLES where ID_TABLE=:id");
    query.bindValue(":id",res);

    return query.exec();
}

#include "invites.h"
#include <QMessageBox>
#include <QPdfWriter>
#include <QPainter>
#include <QSystemTrayIcon>
#include <QDateTime>

invites::invites(QString nom , QString prenom ,int id ,int membre,QString appartenance,QString email )
{ this->nom=nom;
    this->prenom=prenom;
    this->id=id;
    this->membre=membre;
    this->appartenance=appartenance;
    this->email=email;
}


void invites::setid(int  c){id=c;}
void invites::setnom(QString n){nom=n;}
void invites::setprenom(QString m){prenom=m;}
void invites::setemail(QString m){email=m;}


int invites::getid(){return id ;}
QString invites::getnom(){return nom ;}
QString invites::getprenom(){return prenom ;}


bool invites::ajouter()
{
    QSqlQuery query;
    QString res = QString::number(id);
    QString res2=QString::number(membre);
    query.prepare("insert into INVITEES (nom ,prenom , id,APARTENANCE,MEMEBRE,EMAIL) " "values( :nom, :prenom, :id,:APARTENANCE,:MEMEBRE,:EMAIL)");

    query.bindValue(":id",res);
    query.bindValue(":prenom",prenom);
    query.bindValue(":nom",nom);
    query.bindValue(":APARTENANCE",appartenance);
    query.bindValue(":MEMEBRE",res2);
    query.bindValue(":EMAIL",email);

 return query.exec();
}



bool invites::modifier()
{

    QSqlQuery query;
    QString res = QString::number(id);
    QString res2=QString::number(membre);
    query.prepare("update INVITEES set NOM=:nom,PRENOM=:prenom,EMAIL=:email,APARTENANCE=:app,MEMEBRE=:membre where ID=:id");

    query.bindValue(":id",res);
    query.bindValue(":prenom",prenom);
    query.bindValue(":nom",nom);
    query.bindValue(":app",appartenance);
    query.bindValue(":membre",res2);
    query.bindValue(":email",email);

 return query.exec();


}

void invites::imprimer_pdf_invitee_un_seul()
{

                    QDateTime datecreation = QDateTime::currentDateTime();
                    QString afficheDC = "Date de Creation PDF : " + datecreation.toString() ;
                           QPdfWriter pdf("C:\\Users\\Youssef El Mili\\Desktop\\gestion_invitess_tables\\file_invitee_indiv.pdf");
                           QPainter painter(&pdf);
                          int i = 4000;
                               painter.setPen(Qt::red);
                               painter.setFont(QFont("ATLAS", 30));
                               painter.drawText(4000,1500,"Liste Des Invitées");
                               painter.setPen(Qt::black);
                               painter.setFont(QFont("Arial", 15));
                               painter.drawText(100,12500,afficheDC);
                               painter.drawRect(2600,70,7000,2600);
                               QPixmap image(":/Images/images/logo.png");

                               painter.drawPixmap(QRect(0,70,2600,2600),image);
                               painter.drawRect(0,3000,9600,500);
                               painter.setFont(QFont("Arial", 9));
                               painter.drawText(200,3300,"ID");
                               painter.drawText(1300,3300,"NOM");
                               painter.drawText(2500,3300,"PRENOM");
                               painter.drawText(3700,3300,"EMAIL");
                               painter.drawText(6300,3300,"APARTENANCE");
                               painter.drawText(8000,3300,"MEMEBRE");



                               QSqlQuery query;
                               query.prepare("select * from INVITEES");
                               query.exec();
                               while (query.next())
                               {
                                   painter.drawText(200,i,query.value(0).toString());
                                   painter.drawText(1300,i,query.value(1).toString());
                                   painter.drawText(2500,i,query.value(2).toString());
                                   painter.drawText(3700,i,query.value(3).toString());
                                   painter.drawText(6300,i,query.value(4).toString());
                                   painter.drawText(8000,i,query.value(5).toString());

                                  i = i + 450;
                               }


            int reponse = QMessageBox::Yes;
                if (reponse == QMessageBox::Yes)
                {
                    QSystemTrayIcon *notifyIcon = new QSystemTrayIcon;
                    notifyIcon->show();
                    notifyIcon->setIcon(QIcon("icone.png"));

                    notifyIcon->showMessage("Tables des invitées","La liste est prete",QSystemTrayIcon::Information,15000);

                    painter.end();
                }
                if (reponse == QMessageBox::No)
                {
                     painter.end();
                }

}



bool invites::supprimer(int id )
{
    QSqlQuery query ;
    QString res=QString::number(id);

    query.prepare("Delete from INVITEES where id=:id");
    query.bindValue(":id",res);

    return query.exec();
}


QSqlQueryModel * invites::afficher()
{
    QSqlQueryModel *model =new QSqlQueryModel();

    model->setQuery("select *from INVITEES");
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("NOM"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("PRENOM"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("EMAIL"));
    model->setHeaderData(4,Qt::Horizontal,QObject::tr("APARTENANCE"));
    model->setHeaderData(5,Qt::Horizontal,QObject::tr("MEMEBRE"));

    return model ;
}

QSqlQueryModel *invites::afficher(QString checki ,QString type)
{

    QSqlQueryModel *model =new QSqlQueryModel();
    QSqlQuery query ;
    if ( type=="Membre") { type="MEMEBRE";}
    if (type=="SANS CONDITION"){model->setQuery("Select * from INVITEES where  ID like '%"+checki+"%' or EMAIL like '%"+checki+"%' or MEMEBRE like '%"+checki+"%' or PRENOM like '%"+checki+"%' or NOM like '%"+checki+"%' or APARTENANCE like '%"+checki+"%' ");;}

      else model->setQuery("Select * from INVITEES where  "+type+" like '%"+checki+"%' ");



    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("NOM"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("PRENOM"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("EMAIL"));
    model->setHeaderData(4,Qt::Horizontal,QObject::tr("APARTENANCE"));
    model->setHeaderData(5,Qt::Horizontal,QObject::tr("MEMEBRE"));
    return model ;
}

QSqlQueryModel *invites::afficher_trier( QString type,QString facon)
{    QSqlQueryModel *model =new QSqlQueryModel();
     QSqlQuery query ;
       if ( type=="Membre") { type="MEMEBRE";}

      model->setQuery("SELECT * FROM INVITEES ORDER BY "+type+" "+facon+"");

       model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
       model->setHeaderData(1,Qt::Horizontal,QObject::tr("NOM"));
       model->setHeaderData(2,Qt::Horizontal,QObject::tr("PRENOM"));
       model->setHeaderData(3,Qt::Horizontal,QObject::tr("EMAIL"));
       model->setHeaderData(4,Qt::Horizontal,QObject::tr("APARTENANCE"));
       model->setHeaderData(5,Qt::Horizontal,QObject::tr("MEMEBRE"));
       return model ;

}








#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include "connection.h"




int main(int argc, char *argv[])
{   QApplication a(argc, argv);
    MainWindow w;
    Connection c;
    bool test=c.createconnect();
    if(test)
    {
        QMessageBox::information(nullptr, QObject::tr("database is open"),
                    QObject::tr("connection successful.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);

}
    else
        QMessageBox::critical(nullptr, QObject::tr("database is not open"),
                    QObject::tr("connection failed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);



    //return a.exec();
    //w.setStyleSheet("background-image:url(:/Images/images/getting-married-in-spain-guide.jpg)");
    w.showMaximized();

    return a.exec();
}


#include "stat_des_tables.h"
#include "ui_stat_des_tables.h"
#include "connection.h"

stat_des_tables::stat_des_tables(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::stat_des_tables)
{
    ui->setupUi(this);
    QPieSeries *series =new QPieSeries ;

    QList<qreal> list= stat_tables();



        series->append("Ami de la femme",list[0]);
        series->append("Ami du mari",list[1]);
        series->append("Famille de la femme",list[2]);
        series->append("Famille du mari",list[3]);



        QChart *chart =new QChart;
        chart->addSeries(series);
        chart->setTitle("STATS des Tables");
         QChartView *chartview= new QChartView(chart);
         chartview->setParent(ui->horizontalFrame);
}

stat_des_tables::~stat_des_tables()
{
    delete ui;
}

QList<qreal> stat_des_tables::stat_tables()
{  QList<qreal> list ;
    QSqlQuery query;
    query.prepare("select APPARTENANCE_TABLE, sum(NOMBRE_DE_CHAISES) as TotalSum1 from TABLES group by APPARTENANCE_TABLE ORDER BY APPARTENANCE_TABLE ASC");
    query.exec();
    while(query.next())
    {list.append(query.value(1).toInt());}
    return list ;
}



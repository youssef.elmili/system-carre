#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSound>
#include <QtWidgets/QMessageBox>
#include <QFileDialog>
#include <QDialog>
#include <QtCharts>
#include <QWidget>
#include <QFrame>
#include <QMediaPlayer>
#include "invites.h"
#include "tables.h"
#include "planning.h"
#include <QChartView>
#include <QPieSeries>
#include <QPrinter>
#include <QPrintDialog>
#include "dialog.h"
#include "smtp.h"
#include "stat_des_tables.h"
#include "promotion.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_on_off_clicked();
    void sendMail(QString email);
    void mailSent(QString);


    void on_bouton_supprimer_clicked();

    void on_pushButton_2_clicked();

    void on_tableView_activated(const QModelIndex &index);

    void on_pushButton_4_clicked();

    void on_pushButton_6_clicked();

    void on_tableView_2_activated(const QModelIndex &index);

    void on_bouton_supprimer_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_7_clicked();


    void on_pushButton_rechercher_invitee_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_rechercher_table_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_13_clicked();

    void on_pushButton_envoyer_mail_clicked();

    void on_on_off_2_clicked();

    void on_pushButton_40_clicked();

    void on_pushButton_41_clicked();

    void on_pushButton_42_clicked();

    void on_pushButton_44_clicked();

    void on_pushButton_45_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_pushButton_43_clicked();

    void on_pushButton_49_clicked();

    void on_pushButton_46_clicked();



    void on_pushButton_47_clicked();

    void on_pushButton_50_clicked();

    void on_pushButton_48_clicked();

    void on_pushButton_51_clicked();

    void on_lineEdit_2_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    invites inv ;
    tables tab;
    Dialog *dialog;
    stat_des_tables *stat_table;
    planning plan;
    QSound *son;
    Promotion prom;
};
#endif // MAINWINDOW_H

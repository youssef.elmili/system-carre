#include "mainwindow.h"
#include <QMessageBox>
#include <QMovie>
#include <QTableView>

#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "dialog.h"
#include <QPixmap>
#include "invites.h"
#include "tables.h"
#include "smtp.h"





MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

        son=new QSound(":/Images/images/button.wav");
        QPixmap bkgnd(":/Images/images/getting-married-in-spain-guide.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

    QPixmap image(":/Images/images/logo.png");
    int w =ui->label_image->width();
    int h =ui->label_image->height();

    ui->label_image->setPixmap(image.scaled(w,h,Qt::KeepAspectRatio));


    QPixmap image1(":/Images/images/logo.png");
    int w1 =ui->label_image_2->width();
    int h1 =ui->label_image_2->height();

    ui->label_image_2->setPixmap(image.scaled(w1,h1,Qt::KeepAspectRatio));




    QMovie *movie = new QMovie("C:/Users/Youssef El Mili/Desktop/gestion_des_clients/images/source.gif");
    //QLabel *processLabel = new QLabel(this);
    int w2 =ui->label_image_gif->width();
    int h2 =ui->label_image_gif->height();
    movie->setScaledSize(QSize().scaled(w2,h2,Qt::KeepAspectRatio));
    ui->label_image_gif->setMovie(movie);
    ui->label_image_gif_3->setMovie(movie);

    movie->start();


    ui->comboBox_membres->addItem(QIcon(":/Images/images/1.png"),QString::number(1));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/2.png"),QString::number(2));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/3.png"),QString::number(3));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/4.png"),QString::number(4));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/5.png"),QString::number(5));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/6.png"),QString::number(6));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/7.png"),QString::number(7));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/8.png"),QString::number(8));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/9.png"),QString::number(9));
    ui->comboBox_membres->addItem(QIcon(":/Images/images/10.png"),QString::number(10));


    ui->comboBox_appartenance->addItem("Famille du mari");
    ui->comboBox_appartenance->addItem( "Ami du mari");
    ui->comboBox_appartenance->addItem("Famille de la femme");
    ui->comboBox_appartenance->addItem("Ami de la femme");


    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/1.png"),QString::number(1));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/2.png"),QString::number(2));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/3.png"),QString::number(3));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/4.png"),QString::number(4));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/5.png"),QString::number(5));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/6.png"),QString::number(6));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/7.png"),QString::number(7));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/8.png"),QString::number(8));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/9.png"),QString::number(9));
    ui->comboBox_nombre_de_chaises->addItem(QIcon(":/Images/images/10.png"),QString::number(10));


    ui->comboBox_appartenance__invitees->addItem("Famille du mari");
    ui->comboBox_appartenance__invitees->addItem( "Ami du mari");
    ui->comboBox_appartenance__invitees->addItem("Famille de la femme");
    ui->comboBox_appartenance__invitees->addItem("Ami de la femme");


    ui->comboBox_recherche_invitee->addItem("SANS CONDITION");
    ui->comboBox_recherche_invitee->addItem("ID");
    ui->comboBox_recherche_invitee->addItem("Email");
    ui->comboBox_recherche_invitee->addItem("Membre");
    ui->comboBox_recherche_invitee->addItem("Prenom");
    ui->comboBox_recherche_invitee->addItem("Nom");
    ui->comboBox_recherche_invitee->addItem("Apartenance");


    ui->comboBox_trier_invitees->addItem("ID");
    ui->comboBox_trier_invitees->addItem("Email");
    ui->comboBox_trier_invitees->addItem("Membre");
    ui->comboBox_trier_invitees->addItem("Prenom");
    ui->comboBox_trier_invitees->addItem("Nom");
    ui->comboBox_trier_invitees->addItem("Apartenance");


    ui->comboBox_recherche_table->addItem("SANS CONDITION");
    ui->comboBox_recherche_table->addItem("ID_TABLE");
    ui->comboBox_recherche_table->addItem("NUMERO");
    ui->comboBox_recherche_table->addItem("NOMBRE_DE_CHAISES");
    ui->comboBox_recherche_table->addItem("APPARTENANCE_TABLE");


    ui->comboBox_trier_table->addItem("ID_TABLE");
    ui->comboBox_trier_table->addItem("NUMERO");
    ui->comboBox_trier_table->addItem("NOMBRE_DE_CHAISES");
    ui->comboBox_trier_table->addItem("APPARTENANCE_TABLE");


    ui->comboBox_planing->addItem("ID_PLANNING");
    ui->comboBox_planing->addItem( "DESCRIPTION");
    ui->comboBox_planing->addItem("LIEU");
    ui->comboBox_planing->addItem("NOM");


    ui->comboBox_promotion->addItem("ID_PROMOTION");
    ui->comboBox_promotion->addItem( "DATE_POMO");
    ui->comboBox_promotion->addItem("PROMOTION");



   ui->tableView->setModel(inv.afficher()) ;

    ui->lineEdit_cin->setValidator(new QIntValidator(0,9999,this));
    ui->lineEdit_ID_invitees->setValidator(new QIntValidator(0,9999,this));
   // connect(ui->pushButton_envoyer_mail, SIGNAL(clicked()),this, SLOT(sendMail()));




}


void MainWindow::mailSent(QString status)
{
    if(status == "Message sent")
        QMessageBox::warning( 0, tr( "Qt Simple SMTP client" ), tr( "Message sent!\n\n" ) );
}



void MainWindow::sendMail(QString email)
{
    Smtp* smtp = new Smtp("youssef.elmili@esprit.tn", "191JMT2845", "smtp.gmail.com", 465);
    //connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));


          smtp->sendMail("yousssef.elmili@esprit.tn", email ,"invitation","marhbe bikom ");


   //smtp->sendMail("yousssef.elmili@esprit.tn", ui->rcpt->text() , ui->subject->text(),ui->msg->toPlainText());
}




MainWindow::~MainWindow()
{
    delete ui;
}





void MainWindow::on_pushButton_clicked()
{  son->play();
int id=ui->lineEdit_cin->text().toInt();
QString nom=ui->lineEdit_nom->text();
QString prenom=ui->lineEdit_prenom->text();
QString email=ui->lineEdit_email->text();
QString appartenance=ui->comboBox_appartenance->currentText();
int membre=ui->comboBox_membres->currentText().toInt();
invites I(nom,prenom,id,membre,appartenance,email);
bool test=I.ajouter();
 if (test)
 {   ui->tableView->setModel(inv.afficher()) ;
     QMessageBox::information(nullptr,QObject::tr("ok"),
             QObject::tr("ajout effectue\n"
                         "Clich cancel to exit ."),QMessageBox::Cancel );
 }
 else
     QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                 QObject::tr("connection failed.\n"
                             "Click Cancel to exit."), QMessageBox::Cancel);
 ui->lineEdit_cin->setText("");
 ui->lineEdit_nom->setText("");
 ui->lineEdit_prenom->setText("");
 ui->lineEdit_email->setText("");
}





void MainWindow::on_pushButton_3_clicked()
{    son->play();
    QString filename = QFileDialog::getOpenFileName(this, tr("choose"),"",tr("images(*.png *.jpg *.jpeg *.bmp *.gif)"));
    if (QString::compare(filename,QString())!=0)
    {
        QImage image ;
        bool valid =image.load(filename);

        if (valid)
        {   image=image.scaledToWidth(ui->label_image_choisie->width(),Qt::SmoothTransformation);
            ui->label_image_choisie->setPixmap(QPixmap::fromImage(image));
        }
    }
}





void MainWindow::on_on_off_clicked()
{   son->play();
exit(EXIT_SUCCESS);
}




void MainWindow::on_bouton_supprimer_clicked()
{   son->play();
    int id=ui->lineEdit_cin->text().toInt();
    bool test =inv.supprimer(id);
    if (test)
    {  ui->tableView->setModel(inv.afficher()) ;
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("yspr effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("suprfailed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
    ui->lineEdit_cin->setText("");
    ui->lineEdit_nom->setText("");
    ui->lineEdit_prenom->setText("");
    ui->lineEdit_email->setText("");
}




void MainWindow::on_pushButton_2_clicked()
{   son->play();
    ui->tableView->setModel(inv.afficher()) ;
}

void MainWindow::on_tableView_activated(const QModelIndex &index)
{   QString val=ui->tableView->model()->data(index).toString();
    QSqlQuery query;
    query.prepare("select * from INVITEES where PRENOM='"+val+"' or ID='"+val+"' or EMAIL='"+val+"' or MEMEBRE='"+val+"' or NOM='"+val+"' or APARTENANCE='"+val+"' ");
   if (query.exec())
   {  while(query.next())
       {
           ui->lineEdit_cin->setText(query.value(0).toString());
           ui->lineEdit_nom->setText(query.value(1).toString());
           ui->lineEdit_prenom->setText(query.value(2).toString());
           ui->lineEdit_email->setText(query.value(3).toString());
           ui->comboBox_appartenance->setCurrentText(query.value(4).toString());
           ui->comboBox_membres->setCurrentText(query.value(5).toString());

                  }
   }
}





void MainWindow::on_pushButton_4_clicked()
{   son->play();
    int id=ui->lineEdit_cin->text().toInt();
    QString nom=ui->lineEdit_nom->text();
    QString prenom=ui->lineEdit_prenom->text();
    QString email=ui->lineEdit_email->text();
    QString appartenance=ui->comboBox_appartenance->currentText();
    int membre=ui->comboBox_membres->currentText().toInt();

    invites I(nom,prenom,id,membre,appartenance,email);
    bool test=I.modifier();
     if (test)
     {   ui->tableView->setModel(inv.afficher()) ;
         QMessageBox::information(nullptr,QObject::tr("ok"),
                 QObject::tr("ajout effectue\n"
                             "Clich cancel to exit ."),QMessageBox::Cancel );
     }
     else
         QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                     QObject::tr("connection failed.\n"
                                 "Click Cancel to exit."), QMessageBox::Cancel);

     ui->lineEdit_cin->setText("");
     ui->lineEdit_nom->setText("");
     ui->lineEdit_prenom->setText("");
     ui->lineEdit_email->setText("");
}

void MainWindow::on_pushButton_6_clicked()
{  son->play();
    int id=ui->lineEdit_ID_invitees->text().toInt();
    int numero=ui->lineEdit_numero_invitees->text().toInt();
    int nombre=ui->comboBox_nombre_de_chaises->currentText().toInt();
    QString appartenance=ui->comboBox_appartenance__invitees->currentText();
    tables T(id,numero,nombre,appartenance);
    bool test=T.ajouter_table();
     if (test)
     {   ui->tableView_2->setModel(T.afficher_table()) ;
         QMessageBox::information(nullptr,QObject::tr("ok"),
                 QObject::tr("ajout effectue\n"
                             "Clich cancel to exit ."),QMessageBox::Cancel );
     }
     else
         QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                     QObject::tr("connection failed.\n"
                                 "Click Cancel to exit."), QMessageBox::Cancel);
     ui->lineEdit_ID_invitees->setText("");
     ui->lineEdit_numero_invitees->setText("");

}

void MainWindow::on_tableView_2_activated(const QModelIndex &index)
{   son->play();
    QString val=ui->tableView_2->model()->data(index).toString();
       QSqlQuery query;
       query.prepare("select * from TABLES where ID_TABLE='"+val+"' or NUMERO='"+val+"' or NOMBRE_DE_CHAISES='"+val+"' or APPARTENANCE_TABLE='"+val+"'");
      if (query.exec())
      {  while(query.next())
          {
              ui->lineEdit_ID_invitees->setText(query.value(0).toString());
              ui->lineEdit_numero_invitees->setText(query.value(1).toString());
              ui->comboBox_nombre_de_chaises->setCurrentText(query.value(2).toString());
              ui->comboBox_appartenance__invitees->setCurrentText(query.value(3).toString());
                     }
                     }
}

void MainWindow::on_bouton_supprimer_2_clicked()
{   son->play();
    bool id=ui->lineEdit_ID_invitees->text().toInt();
    bool test =tab.supprimer_table(id);
    if (test)
    {  ui->tableView_2->setModel(tab.afficher_table()) ;
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("yspr effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("suprfailed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}







void MainWindow::on_pushButton_5_clicked()
{   son->play();
    ui->tableView_2->setModel(tab.afficher_table());

}

void MainWindow::on_pushButton_7_clicked()
{   son->play();
    int id_tab=ui->lineEdit_ID_invitees->text().toInt();
    int  numero=ui->lineEdit_numero_invitees->text().toInt();
    int  nombre =ui->comboBox_nombre_de_chaises->currentText().toInt();
    QString appartenance=ui->comboBox_appartenance__invitees->currentText();
    tables ta(id_tab,numero,nombre,appartenance);
    bool test=ta.modifier_table();
     if (test)
     {   ui->tableView_2->setModel(tab.afficher_table());
         QMessageBox::information(nullptr,QObject::tr("ok"),
                 QObject::tr("ajout effectue\n"
                             "Clich cancel to exit ."),QMessageBox::Cancel );
     }
     else
         QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                     QObject::tr("connection failed.\n"
                                 "Click Cancel to exit."), QMessageBox::Cancel);

     ui->lineEdit_cin->setText("");
     ui->lineEdit_nom->setText("");
     ui->lineEdit_prenom->setText("");
}


void MainWindow::on_pushButton_rechercher_invitee_clicked()
{  son->play();
    QString type=ui->comboBox_recherche_invitee->currentText();
    QString chaine=ui->lineEdit_recherche_invitee->text();
    ui->tableView->setModel(inv.afficher(chaine,type));
}



void MainWindow::on_pushButton_8_clicked()
{ son->play();
    dialog=new Dialog(this);
    dialog->show();
}

void MainWindow::on_pushButton_9_clicked()
{   son->play();
    if(ui->radioButton_trier_invitees_AZ->isChecked()){
    QString type=ui->comboBox_trier_invitees->currentText();
        ui->tableView->setModel(inv.afficher_trier(type,"ASC"));}

    if(ui->radioButton_trier_invitees_ZA->isChecked()){
        QString type=ui->comboBox_trier_invitees->currentText();
            ui->tableView->setModel(inv.afficher_trier(type,"DESC"));}
}




void MainWindow::on_pushButton_rechercher_table_clicked()
{    son->play();
       QString type=ui->comboBox_recherche_table->currentText();
        QString chaine=ui->lineEdit_recherche_table->text();
        ui->tableView_2->setModel(tab.afficher(chaine,type));
}

void MainWindow::on_pushButton_10_clicked()
{   son->play();
    if(ui->radioButton_trier_table_AZ->isChecked()){
        QString type=ui->comboBox_trier_table->currentText();
            ui->tableView_2->setModel(tab.afficher_trier(type,"ASC"));}

        if(ui->radioButton_trier_table_ZA->isChecked()){
            QString type=ui->comboBox_trier_table->currentText();
                ui->tableView_2->setModel(tab.afficher_trier(type,"DESC"));}
}

void MainWindow::on_pushButton_11_clicked()
{   son->play();
    stat_table=new stat_des_tables(this);
       stat_table->show();
}




void MainWindow::on_pushButton_12_clicked()
{ son->play();
    inv.imprimer_pdf_invitee_un_seul();
}




void MainWindow::on_pushButton_13_clicked()
{   son->play();
   tab.imprimer_table_pdf_tables();
}

void MainWindow::on_pushButton_envoyer_mail_clicked()
{QSqlQuery query;
    QList <QString> list ;
      query.prepare("select email from invitees");
      query.exec();
      while(query.next())
      {list.append(query.value(0).toString());}
      int i=0;
      while(i<4)
      {
    sendMail(list[i]);
    i=i+1;
      }
}

void MainWindow::on_on_off_2_clicked()
{
    son->play();
    exit(EXIT_SUCCESS);
}

void MainWindow::on_pushButton_40_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();


    int id =ui->lineEdit_id_planning->text().toInt();
    QString descri =ui->lineEdit_description_planning->text();
    QString lieu =ui->lineEdit_lieu_planning->text();
    QString nom =ui->lineEdit_nom_planning->text();

    planning pla(id,descri,lieu,nom);
    bool test = pla.ajouter();
    if (test)
    {   ui->tableView_planning->setModel(plan.afficher()) ;
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("ajout effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("connection failed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}



void MainWindow::on_pushButton_41_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();

    int id=ui->lineEdit_id_planning->text().toInt();


    bool test=plan.supprimer(id);
    if (test)
    {  ui->tableView_planning->setModel(plan.afficher()) ;
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("supressin effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }

    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("suprfailed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}




void MainWindow::on_pushButton_42_clicked()
{/*sound*/
    QMediaPlayer *player = new QMediaPlayer;

    player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
    player->setVolume(100);
    player->play();

    int id=ui->lineEdit_id_planning->text().toInt();
     QString descr=ui->lineEdit_description_planning->text();
     QString lieu=ui->lineEdit_lieu_planning->text();
     QString nom=ui->lineEdit_nom_planning->text();

     planning pla(id,descr,lieu,nom);
     bool test=pla.modifier();
     if (test)
     {   ui->tableView_planning->setModel(plan.afficher());
         QMessageBox::information(nullptr,QObject::tr("ok"),
                 QObject::tr("modification effectue\n"
                             "Clich cancel to exit ."),QMessageBox::Cancel );
     }
     else
         QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                     QObject::tr("connection failed.\n"
                                 "Click Cancel to exit."), QMessageBox::Cancel);

     ui->lineEdit_id_planning->setText("");
     ui->lineEdit_nom_planning->setText("");
     ui->lineEdit_lieu_planning->setText("");
     ui->lineEdit_description_planning->setText("");


}





void MainWindow::on_pushButton_44_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();

    ui->tableView_planning->setModel(plan.afficher());
}

void MainWindow::on_pushButton_45_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();


    QString critere=ui->comboBox_planing->currentText();
           QString mode;
           if (ui->checkBox_DESC_planning->checkState()==false)
       {
                mode="DESC";
       }
            else if(ui->checkBox_ASC_planning->checkState()==false)
            {
                mode="ASC";
            }
       ui->tableView_planning->setModel(plan.trie(critere,mode));
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    if(ui->lineEdit->text() == "")
            {
                ui->tableView_planning->setModel(plan.afficher());
            }
            else
            {
                 ui->tableView_planning->setModel(plan.rechercher(arg1));
            }
}

void MainWindow::on_pushButton_43_clicked()
{
    plan.genererPDFplan();
}

void MainWindow::on_pushButton_49_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();

    int id =ui->lineEdit_id_promotion_2->text().toInt();
    QString date =ui->lineEdit_date_de_promotion_2->text();
    QString promo =ui->lineEdit_promotion_promotion_2->text();

    Promotion pro(id,date,promo);
    bool test = pro.ajouter();
    if (test)
    {  ui->tableView_promotion->setModel(prom.afficher());
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("ajout effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("connection failed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}






void MainWindow::on_pushButton_46_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();

    int id =ui->lineEdit_id_promotion_2->text().toInt();

    bool test=prom.supprimer(id);
    if (test)
    {  ui->tableView_promotion->setModel(prom.afficher());
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("ajout effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("connection failed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}

void MainWindow::on_pushButton_47_clicked()
{
    /*sound*/
    QMediaPlayer *player = new QMediaPlayer;

    player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
    player->setVolume(100);
    player->play();

    int id =ui->lineEdit_id_promotion_2->text().toInt();
    QString date =ui->lineEdit_date_de_promotion_2->text();
    QString promo =ui->lineEdit_promotion_promotion_2->text();
    Promotion pro(id,date,promo);
    bool test=pro.modifier();


    if (test)
    {  ui->tableView_promotion->setModel(prom.afficher());
        QMessageBox::information(nullptr,QObject::tr("ok"),
                QObject::tr("ajout effectue\n"
                            "Clich cancel to exit ."),QMessageBox::Cancel );
    }
    else
        QMessageBox::critical(nullptr, QObject::tr("Not ok "),
                    QObject::tr("connection failed.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
}

void MainWindow::on_pushButton_50_clicked()
{
    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();

    ui->tableView_promotion->setModel(prom.afficher());
}




void MainWindow::on_pushButton_48_clicked()
{
    /*impression pdf*/

    /*sound*/
       QMediaPlayer *player = new QMediaPlayer;

       player->setMedia(QUrl::fromLocalFile(":/Images/images/button-8.mp3"));
       player->setVolume(100);
       player->play();


          {
              QString strStream;
                      QTextStream out(&strStream);
                      const int rowCount = ui->tableView_promotion->model()->rowCount();
                      const int columnCount =ui->tableView_promotion->model()->columnCount();

                      out <<  "<html>\n"
                              "<head>\n"
                              "<meta Content=\"Text/html; charset=Windows-1251\">\n"
                              <<  QString("<title>%1</title>\n").arg("PROMOTION")
                              <<  "</head>\n"
                              "<body bgcolor=#D3D3D3 link=#5000A0>\n"
                                  "<img src='C:/Users/user2/Desktop/2A/QT/CRUD_EVENT/logo_groupe' width='100' height='100'>\n"
                                  "<h1>Liste de promotion </h1>"



                                  "<table border=1 cellspacing=0 cellpadding=2>\n";


                      // headers
                          out << "<thead><tr bgcolor=#f0f0f0>";
                          for (int column = 0; column < columnCount; column++)
                              if (!ui->tableView_promotion->isColumnHidden(column))
                                  out << QString("<th>%1</th>").arg(ui->tableView_promotion->model()->headerData(column, Qt::Horizontal).toString());
                          out << "</tr></thead>\n";
                          // data table
                             for (int row = 0; row < rowCount; row++) {
                                 out << "<tr>";
                                 for (int column = 0; column < columnCount; column++) {
                                     if (!ui->tableView_promotion->isColumnHidden(column)) {
                                         QString data = ui->tableView_promotion->model()->data(ui->tableView_promotion->model()->index(row, column)).toString().simplified();
                                         out << QString("<td bkcolor=0>%1</td>").arg((!data.isEmpty()) ? data : QString("&nbsp;"));
                                     }
                                 }
                                 out << "</tr>\n";
                             }
                             out <<  "</table>\n"
                                 "</body>\n"
                                 "</html>\n";

                             QTextDocument *document = new QTextDocument();
                             document->setHtml(strStream);

                             QPrinter printer;

                             QPrintDialog *dialog = new QPrintDialog(&printer, NULL);
                             if (dialog->exec() == QDialog::Accepted) {
                                 document->print(&printer);
                          }
          }
}

void MainWindow::on_pushButton_51_clicked()
{
    QString critere=ui->comboBox_promotion->currentText();
           QString mode;
           if (ui->checkBox->checkState()==false)
       {
                mode="DESC";
       }
            else if(ui->checkBox_2->checkState()==false)
            {
                mode="ASC";
            }
       ui->tableView_promotion->setModel(prom.trie(critere,mode));
}




void MainWindow::on_lineEdit_2_textChanged(const QString &arg1)
{
    if(ui->lineEdit_2->text() == "")
            {
                ui->tableView_promotion->setModel(prom.afficher());
            }
            else
            {
                 ui->tableView_promotion->setModel(prom.rechercher(arg1));
            }
}

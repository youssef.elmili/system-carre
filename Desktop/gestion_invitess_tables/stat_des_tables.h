#ifndef STAT_DES_TABLES_H
#define STAT_DES_TABLES_H

#include <QDialog>
#include <QtCharts>
#include <QChartView>
#include <QPieSeries>
#include <QWidget>
#include <QList>

namespace Ui {
class stat_des_tables;
}

class stat_des_tables : public QDialog
{
    Q_OBJECT

public:
    explicit stat_des_tables(QWidget *parent = nullptr);
    ~stat_des_tables();

private:
    Ui::stat_des_tables *ui;
    QList<qreal> stat_tables();
};

#endif // STAT_DES_TABLES_H

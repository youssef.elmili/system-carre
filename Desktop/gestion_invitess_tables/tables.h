#ifndef TABLES_H
#define TABLES_H
#include <QString>
#include <QSqlQuery>
#include <QSqlQueryModel>


class tables
{ int ID_table,numero,nombre_de_chaises;
    QString appartenance_table;

public:
    tables();
    tables(int id , int numero ,int nombre_chaises,QString appartenance);
    void setID(int x){ID_table=x;}
    void setnumero(int x){numero=x;}
    void setnombre_de_chaises(int x){ID_table=nombre_de_chaises=x;}
    void setappartenance_table( QString x){appartenance_table=x;}


    int getID_table(){return ID_table ; }
    int getnumero(){return numero ; }
    int getnombre_de_chaises(){return nombre_de_chaises ; }
    QString getappartenance_table(){return appartenance_table ; }



    bool ajouter_table();
    QSqlQueryModel * afficher_table();
    QSqlQueryModel *afficher (QString checki,QString type);
    QSqlQueryModel *afficher_trier (QString type,QString facon);
    bool supprimer_table(int id) ;
    bool modifier_table();
    void imprimer_table_pdf_tables();

};

#endif // TABLES_H

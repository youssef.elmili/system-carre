QT       += core gui charts sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia printsupport

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    connection.cpp \
    dialog.cpp \
    invites.cpp \
    main.cpp \
    mainwindow.cpp \
    planning.cpp \
    promotion.cpp \
    smtp.cpp \
    stat_des_tables.cpp \
    tables.cpp

HEADERS += \
    connection.h \
    dialog.h \
    invites.h \
    mainwindow.h \
    planning.h \
    promotion.h \
    smtp.h \
    stat_des_tables.h \
    tables.h

FORMS += \
    dialog.ui \
    mainwindow.ui \
    stat_des_tables.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    MyFiles.qrc
